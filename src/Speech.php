<?php

namespace AzureSpring\Baidu\Yuyin;

class Speech implements SpeechInterface
{
    private $stream;

    private $format;


    public function __construct( /* string */ $filename, FFMpegFactoryInterface $factory )
    {
        $this->filename = $filename;

        $p = $factory->createFFProbe();
        $this->stream = $p->streams( $filename )->audios()->first();
        $this->format = $p->format( $filename );
    }

    public function getFormatName()
    {
        return $this->format->get( 'format_name' );
    }

    public function getSampleRate()
    {
        return $this->stream->get( 'sample_rate' );
    }

    public function getChannels()
    {
        return $this->stream->get( 'channels' );
    }

    public function getData()
    {
        return file_get_contents( $this->filename );
    }
}
