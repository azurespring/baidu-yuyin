<?php

namespace AzureSpring\Baidu\Yuyin;

interface SpeechInterface
{
    /**
     * @return string
     */
    public function getFormatName();

    /**
     * @return integer
     */
    public function getSampleRate();

    /**
     * @return integer
     */
    public function getChannels();

    /**
     * @return string
     */
    public function getData();
}
