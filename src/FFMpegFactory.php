<?php

namespace AzureSpring\Baidu\Yuyin;

class FFMpegFactory implements FFMpegFactoryInterface
{
    private $ffmpeg_home;


    public function __construct( $ffmpeg_home = null )
    {
        $this->ffmpeg_home = $ffmpeg_home;
    }

    public function createFFMpeg()
    {
        return \FFMpeg\FFMpeg::create(!$this->ffmpeg_home ? [] : [
            'ffmpeg.binaries'   => "{$this->ffmpeg}/bin/ffmpeg",
            'ffprobe.binaries'  => "{$this->ffmpeg}/bin/ffmpeg",
        ]);
    }

    public function createFFProbe()
    {
        return \FFMpeg\FFProbe::create(!$this->ffmpeg_home ? [] : [
            'ffmpeg.binaries'   => "{$this->ffmpeg}/bin/ffmpeg",
            'ffprobe.binaries'  => "{$this->ffmpeg}/bin/ffmpeg",
        ]);
    }
}
