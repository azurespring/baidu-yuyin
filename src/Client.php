<?php

namespace AzureSpring\Baidu\Yuyin;

class Client
{
    private $cuid;

    private $token;

    private $guzzle;


    public function __construct( /* string */ $cuid, /* string */ $token, \GuzzleHttp\Client $guzzle )
    {
        $this->cuid   = $cuid;
        $this->token  = $token;
        $this->guzzle = $guzzle;
    }

    public function analyze( Speech $speech )
    {
        $response = $this->guzzle->post(
            'http://vop.baidu.com/server_api', [
                'json' => [
                    'format'  => $speech->getFormatName(),
                    'rate'    => $speech->getSampleRate(),
                    'channel' => $speech->getChannels(),
                    'cuid'    => $this->cuid,
                    'token'   => $this->token,
                    'speech'  => base64_encode( $speech->getData() ),
                    'len'     => strlen( $speech->getData() )
                ]
            ]);

        $response = json_decode( $response->getBody(), true );
        if (empty( $response['err_no'] ))
            return $response['result'];

        throw new ASRException( $response['err_msg'], $response['err_no'] );
    }
}
