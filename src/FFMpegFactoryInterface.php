<?php

namespace AzureSpring\Baidu\Yuyin;

interface FFMpegFactoryInterface
{
    /**
     * @return \FFMpeg\FFMpeg
     */
    public function createFFMpeg();

    /**
     * @return \FFMpeg\FFProbe
     */
    public function createFFProbe();
}
