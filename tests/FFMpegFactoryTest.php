<?php

namespace AzureSpring\Baidu\Yuyin\Tests;

use PHPUnit\Framework\TestCase;
use AzureSpring\Baidu\Yuyin\FFMpegFactory;

class FFMpegFactoryTest extends TestCase
{
    private $factory;


    public function setUp()
    {
        $this->factory = new FFMpegFactory();
    }

    public function testCreateFFMpeg()
    {
        $this->assertInstanceOf( \FFMpeg\FFMpeg::class, $this->factory->createFFMpeg() );
    }

    public function testCreateFFProbe()
    {
        $this->assertInstanceOf( \FFMpeg\FFProbe::class, $this->factory->createFFProbe() );
    }
}
