<?php

namespace AzureSpring\Baidu\Yuyin;

use PHPUnit\Framework\TestCase;
use AzureSpring\Baidu\Yuyin\Client;
use AzureSpring\Baidu\Yuyin\Speech;
use AzureSpring\Baidu\Yuyin\FFMpegFactory;

class ClientTest
{
    private $guzzle;

    private $client;

    private $speech;


    public function setUp()
    {
        $this->guzzle   = $this->createMock( \GuzzleHttp\Client::class );
        $this->client   = new Client( 'a cuid', 'a token' );
        $this->speech   = new Speech( __DIR__ . '/varian-wrynn.amr', new FFMpegFactory() );
        $this->data     = <<< __SPEECH__
IyFBTVIKPAl/rxskq8fMqGn79wFnc0AAU4UTlshAAAZvd7ir5HA85FpoV+p/AkPMRNQOU7frE2+9C4GvRSwuAPNzc3GjEDzeRDVFCLQRwf9+mvducDsM9g1SF+iss5cwSXMEEIvQPAhBmk/NdJGw7nX1YcJmV2WnjHppFvaxcL0CCa+8xIA84EOGH/mWRtSrb2yk1j5ZI1p30d/HiXhVniHnVU9DQDzeTahV59oi8O7bE5dSBkjssOuWE2gDby2VPdVrS5owPOBDiD7mAhAj3Ba7LJC8rFk7LholgXeGLklnkuFroBA84EArS1qOHUl3oAzRCva6Li4RXu4kksj3RWOdP/dzADzgbDJDWNbHa1RBxOoaXc0B2RT2lrSYm9oBUMOx8VWQPCBAJHjoFkjrVE9E0jOv1R3Kgm2T5HRAKY26LJAB+VA8CD96Fre3AKW7VEcpZVZRiJNNGYCtcHpkwW48lsRWkDzgN4hT+7KJZbsaHBRowjI04DaTRcntv08h3/mpp7UwPOBKLSb/8m7LVQrkqKMsD/oZTLenszTaZp2mIWvmBHA83kA4Vl/8BeH/MsyNSldZBDXlRgkzm86gSDIKAdp50DzYQDV8S+YZksxrx7BF+1Wd9GYckV3tZT0Pho9NYbeAPAQcDVfzoZkS0OQHvkH8BwXUM70yIEXgrI3jBlfNuOA8J4A7xr03udPNp9rEaAJcWldq/+W5XcnWSEDY4cxkIDwWEkiaplJUXCQCm3vGEgF6KUx6BR+GHuGDv4B7w5wAPDBGUcfmbbn6cG9qz6qyo1B1NEFYATj3PjrQuTLgBkA8IChKx+wei80/zbrgxZyJwwly5u6iRVgJsf299x2ecDwqqYan5n9K140qmHkdR9wdKeOrn7Mi/2kyrk08LgDgPAg3qJXvAABJdh7K7kVuReOAFVwkLIFT+K36gEKP+DA83FPe24TVK48VlK8VW585GkWeKskYRZxJqweXaqUy4DwmvAM6a+g7OmYdvHJM50IFCguL2nJ9S40N16cERKywPETIZ7u4XgG0rhlbIYp6okYi4Zp4LqgsLQcmpb1ff8A80GIMEZ6IId4LjeKoWzxNhSM+GD6EUpsH4DaZenbUUDyAciHaIRoQfgKF6woU54W3dw6xcBQM+GQaDrOG5ZDAPLU3dpobeBB+FYYVSUx6xmHXc4Kvzy/fsRTp2poqAzA8Tmz0ziG8Af4TILpOyWmW7CM3yIo7AJKh50rPimZn0DyQ8lPHRWgBvhaDtDgqOqh7OaQGcaHjlutjJhpd8ZRgPFLqk81BgAGeBqEK7BHt3dzAQdXhFGXP0FlkKcKSkfA8tPINwDYcAl4Ngzb5e5VFMnlOjR2WlPYb+Ll9rvxVEDyQ7GlL/Y6BPgrKrDJ07ZVooai4euFFn+9nGqj6d7TAPEhyMP4o5DwaxNTLa3CxYVatxoYr7otosEXQ7SUPaEA8WH7cX+ecAt4Sb12UOI6ak2XD+N/Ba6trJaXNaHKT8DxUe2P+HxwwnhGzHOM5PUsuBmZB7UkZIhNbmQMtr1hwPN5ubx5DjgF+CHvSSnA/gf2ns0oUeq94GBl2195yfQA8PpahPW/qVL4HeFLo0KdxqHllu/o+UdocrWPQ0tAG8Dwoum0n5+YbTxRlLHSxsl7sauCI8kWdmdJBOy07wHQwPB7Mnx/nnge2ksQ8R6TJw+588f1IALxbsgiQ6G6P2TA84DtlQ6BCJ54AW8IDqS56sDmNbLHFvBv8I6h4MvdVcDwaYiOb4IwA7TIVVIgrox8L8BGyMJGf1Xwb7uw6rK5gPN48UFZgkBh6RZHUAhyJRciBuvH6iXY5YMoSAFtkCVA8UnUyn6ru3t4Fjhhw7tdMGyXTFf7FbtI2oKRBX7i2UDxCydOOV0gJPgOaNWzUDntiO5gVzI6vkUfLVPkVv8QwPBrrYOYjbin+C4X7AHYCLEDwgb5N84006t/WmAhhfqA8RKgLiX9eAfaIFSOvjmuv+NndS92fLVBWCXRmL5X2ADw0Zmbvf9yBXhHNghbUdFX56+CP+7NyOYZuhBQ0DnogPHCIFZHGWIS+CzqSStEVOsMwF54eiI71rqCD/CJO9fA8ZOwgk/vmGj4HJQI8WEJXycFaDDopzxOgzDP9yZh7YDyQch/uvPsIPgiUUTsivcy+gv7kq3F4IaumUWZXxEcgPJB2Fyf9lgOeDYA0cqTZpgg3Hhx+nZ8NXaY5YqUn6UA8oveIir8cEP4Gw5VUf8Z3WoaGfowh1Tx7DNp/cZs8ADzQZhVQCwABHgHYS1+6ElVE/IRzgUtFbykE/QPlXw1gPFRblo1RYAH8JlsTCd1dBuDhIY2TZzklp593BGfe2BA8kGWWUBRnUN4ADVwtYh5uIVbFdFzE7310MyPMOvlLIDxUbNyN3/4BrTNuVDdW0qBEqBAX9ogYUKzp14KR6CfwPFJVlY1XewgWmCCi5aqaa9BcZipOPS8RyhLw8P7FrEA8UkPBSqPIEGW7pXXv1+xVcfenkVoloPbsT+qP9TGF4DxKW5BHC7oS9ohdNOx+dv4LeSpV49J9GJWNxgXTIFRQPFJle0fWhwBrVIrlW2scMnVYXX6UcKUcmuglBy1UhJA84FILQdVsC1Srl0ShktgoouMkm4qRD1iE6IagZ/YOwDzYQ5VW/cgUh5h7SzyYhM/DyCKr/9GSnlsLG/ZiG77wPOBCEYsuigPrVNLD41qJ3VaN0WFylvhDeISxM5CzfuA8UkubUqH6AHDmFJsHdqQ1Vg6bekny6ssXEQKneyGGMDzgQZgfezoUsOrX8kGJeSvC5h9slLbnXR1CgFeshpJQPN5LnU/VVhaj3Vq3OmuEvgJr61zetlXySCsoDwuiF3A84EGrJ38WDwllADQLFQHwZKcm7V2sHjOGrou4JO8cYDzgTDVPgE8B0OcBKzw5MoBYqO2FstTyNr19oHY2D3LAPAREOUKBGoTh+sQzKk3kgEQWSG0hKhAG3IN1C9keqfA83j4+G66GoYH+11pWLg7iGvGrYHl36T3MZcZHKaJcgDwIU5838+EJ4fxVJGvfU1DywmC9+WCW9hYJeccNYmhAPAg5oe/n2w2h/rUMW1jZGhMFt27cdFbIRxrh92V0SGA84EogH+1RlKH+0yJULXzNwrc4VdnnftMUKuX5ZPufcDzgO4pP59ppxbkMikB1y6fHMf05EXU0qgyhz6EvGRCwPAhCY0/tFhOQ5WmCN/J9lQq2u+ApfYv9kzhocaG3qLA8AkA+J+dtkfLIDVri0C9JyaUeEZO8IB7+zvL+FUe2UDzgSilH7VJPYf8b5ZN8BsZV6xx/3dLSmzN83Izgvz/QPAQoPke9BBJB5opLX3HBfWkzaVugS1/MBxnjINdo+9A8CDQyl+dBkYGM+WgqZCZYxnCuc4ODJqPGvmzsDnmpIA==
__SPEECH__;
    }

    public function testAnalyzeOk()
    {
        $response = $this->createMock( \GuzzleHttp\Response::class );
        $response
            ->method( 'getBody' )
            ->willReturn( '{"corpus_no":"6390212716960680091","err_msg":"success.","err_no":0,"result":["我的孩子啊，"],"sn":"754165522971487837339"}' );

        $this
            ->guzzle
            ->expects( $this->once() )
            ->method( 'post' )
            ->with(
                'http://vop.baidu.com/server_api', [
                    'json' => [
                        'format'  => 'amr',
                        'rate'    => 8000,
                        'channel' => 1,
                        'cuid'    => 'a cuid',
                        'token'   => 'a token',
                        'speech'  => $this->data,
                        'len'     => strlen( $speech->getData() )
                    ]
                ]
            )
            ->willReturn( $response );

        $this->assertEquals(
            '我的孩子啊，',
            $this->client->analyze( $this->speech ));
    }

    /**
     * @expectedException AzureSpring\Baidu\Yuyin\ASRException
     */
    public function testAnalyzeError()
    {
        $response = $this->createMock( \GuzzleHttp\Response::class );
        $response
            ->method( 'getBody' )
            ->willReturn( '{"err_msg":"recognition error.","err_no":3301,"sn":"190667191791487838166"}' );

        $this
            ->guzzle
            ->expects( $this->once() )
            ->method( 'post' )
            ->with(
                'http://vop.baidu.com/server_api', [
                    'json' => [
                        'format'  => 'amr',
                        'rate'    => 8000,
                        'channel' => 1,
                        'cuid'    => 'a cuid',
                        'token'   => 'a token',
                        'speech'  => $this->data,
                        'len'     => strlen( $speech->getData() )
                    ]
                ]
            )
            ->willReturn( $response );
        $this->client->analyze( $this->speech );
    }
}
