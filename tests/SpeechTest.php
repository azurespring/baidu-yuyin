<?php

namespace AzureSpring\Baidu\Yuyin\Tests;

use PHPUnit\Framework\TestCase;
use AzureSpring\Baidu\Yuyin\Speech;
use AzureSpring\Baidu\Yuyin\FFMpegFactory;

class SpeechTest extends TestCase
{
    private $factory;

    private $speech;


    public function setUp()
    {
        $this->speech = new Speech( __DIR__ . '/varian-wrynn.amr', new FFMpegFactory() );
    }

    public function testGetFormatName()
    {
        $this->assertEquals(
            'amr',
            $this->speech->getFormatName());
    }

    public function testGetSampleRate()
    {
        $this->assertEquals(
            8000,
            $this->speech->getSampleRate());
    }

    public function testGetChannels()
    {
        $this->assertEquals(
            1,
            $this->speech->getChannels());
    }

    public function testGetData()
    {
        $this->assertEquals(
            2566,
            strlen( $this->speech->getData() ));
        $this->assertEquals(
            'a95e49646c63933e82db53d4c2117d60ca6c29ee',
            sha1( $this->speech->getData() ));
    }
}
